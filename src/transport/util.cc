#include "util.h"

std::function<void(int)> signalHandlerFunction;

int transport::randomNumber(int from, int to) {
  std::random_device dev;
  std::mt19937 rng(dev());
  std::uniform_int_distribution<std::mt19937::result_type> distribution(from, to);
  return distribution(rng);
}

// [2, 5, 1, 44, 33, 127] -> checksum = 2 + 5 + 1 + 44 ..
uint64_t transport::computeChecksum(ByteBuffer &buffer) {
  return std::accumulate(buffer.getBytes().begin(), buffer.getBytes().begin() + 8 + 128, 0);
}

std::vector<int> transport::nRandomNumbers(int n, int from, int to) {
  std::vector<int> vec;

  std::random_device dev;
  std::mt19937 rng(dev());
  std::uniform_int_distribution<std::mt19937::result_type> distribution(from, to);

  std::generate_n(std::back_inserter(vec), n, [&]() {
    return distribution(rng);
  });

  return vec;
}

void transport::handleSignal(int signum, std::function<void(int)> handler) {
  signalHandlerFunction = handler;
  signal(signum, signalHandler);
}

void transport::signalHandler(int signum) {
  signalHandlerFunction(signum);
}
