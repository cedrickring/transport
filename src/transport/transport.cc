#include <iostream>
#include <string>
#include "sender/sender.h"
#include "receiver/receiver.h"

void startInputLoop() {
  std::string str;

  while (std::getline(std::cin, str)) {
    if (str == "exit") {
      std::cout << "Stopping..." << std::endl;
      break;
    }
  }
}

/**
 * Entrypoint for the receiver/sender
 *
 * Usage: ./transport <sender|receiver> [...]
 * @param argc argument count
 * @param argv all arguments
 * @return
 */
int main(int argc, char **argv) {
  std::vector<std::string> args;
  args.assign(argv, argv + argc);

  if (args.size() < 2) {
    std::cout << "Usage: ./transport <sender|receiver> [...]" << std::endl;
    return 1;
  }

  if (args[1] == "sender") {
    std::cout << "Starting sender..." << std::endl;
    std::cout << "To stop sending messages, use Ctrl+C." << std::endl;

    Sender sender(args);
    sender.startSendLoop();
    sender.startReceiveLoop();
    sender.startTimeoutLoop();

    transport::handleSignal(SIGINT, [&](int) {
      std::cout << "Stopping sender..." << std::endl;
      sender.stopSendLoop();
      sender.stopTimeoutLoop();
    });

    sender.waitForSendLoop();
    sender.waitForTimeoutLoop();
    sender.stopReceiveLoop();
  } else {
    std::cout << "Starting receiver..." << std::endl;
    std::cout << "To stop receiving messages, use Ctrl+C or type 'exit'." << std::endl;
    Receiver receiver(args);
    receiver.startReceiving();

    transport::handleSignal(SIGINT, [&](int) {
      std::cout << "Stopping receiver..." << std::endl;
      receiver.stopReceiving();
      std::cout << "Stopped. " << std::endl;
      std::exit(0);
    });

    startInputLoop();
    receiver.stopReceiving();
  }

  std::cout << "Stopped." << std::endl;
  return 0;
}