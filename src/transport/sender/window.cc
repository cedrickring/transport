#include "window.h"

Window::Window(uint32_t size, int timeoutInMillis) : size(size), timeoutInMillis(timeoutInMillis) {
}

void Window::acknowledgeMessage(int sequenceNumber) {
  std::lock_guard<std::mutex> guard(messageMutex);

  auto removed = storedMessages.erase(sequenceNumber);
  if (removed > 0) {
    sentMessageTimestamp.erase(sequenceNumber);
    std::cout << "- Acknowledged message with sequence number " << sequenceNumber << std::endl;
  }
}

std::vector<ByteBuffer> Window::getMessages(int fromSequenceNumber) {
  std::lock_guard<std::mutex> guard(messageMutex);
  std::vector<ByteBuffer> buffers;

  milliseconds now = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
  for (int i = fromSequenceNumber; i < nextSequence; ++i) {
    auto it = storedMessages.find(i);
    if (it != storedMessages.end()) {
      sentMessageTimestamp[it->first] = now;
      buffers.push_back(it->second);
    }
  }

  return buffers;
}

std::optional<ByteBuffer> Window::getNextMessage() {
  std::lock_guard<std::mutex> guard(messageMutex);

  // generate messages if there are none
  if (storedMessages.size() == 0 && nextSequence == 1) {
    for (uint32_t i = 1; i <= size; ++i) {
      storedMessages[i] = generateMessageFn(i);
    }
  }

  // generate new messages if window is empty
  auto lastSeqNumberIt = storedMessages.rbegin();
  if (lastSeqNumberIt == storedMessages.rend()) {
    for (uint32_t i = nextSequence; i < nextSequence + size; ++i) {
      storedMessages[i] = generateMessageFn(i);
    }
  }

  milliseconds now = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
  auto it = storedMessages.find(nextSequence);
  if (it == storedMessages.end()) {
    return {};
  }

  ++nextSequence;
  sentMessageTimestamp[it->first] = now;
  return it->second;
}

std::vector<ByteBuffer> Window::getTimeoutedMessages() {
  std::lock_guard<std::mutex> guard(messageMutex);
  std::vector<ByteBuffer> buffers;

  auto now = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
  for (auto &it : sentMessageTimestamp) {
    auto diff = duration_cast<milliseconds>(now - it.second);
    if (diff.count() > timeoutInMillis) {
      buffers.push_back(storedMessages[it.first]);
    }
  }

  return buffers;
}

void Window::setGenerateMessageFn(const std::function<ByteBuffer(int)> generateMessageFn) {
  Window::generateMessageFn = generateMessageFn;
}
