#ifndef TRANSPORT_SRC_TRANSPORT_SENDER_WINDOW_H_
#define TRANSPORT_SRC_TRANSPORT_SENDER_WINDOW_H_

#include <cstdint>
#include <map>
#include <mutex>
#include <functional>
#include <chrono>
#include <optional>

#include "common/byte_buffer.h"

using namespace std::chrono;

class Window {
 public:
  /**
   * Initialize window with size N and message timeout
   * @param size window size N
   * @param timeoutInMillis message timeout in milliseconds
   */
  explicit Window(uint32_t size, int timeoutInMillis = 1000);

  /**
   * Acknowledge sequence
   * @param sequenceNumber number of sequence
   */
  void acknowledgeMessage(int sequenceNumber);

  /**
   * Get messages from `fromSequenceNumber` to `nextSequence`
   * <br>
   * Resets the timeout of the message
   * @param fromSequenceNumber lower bound of sequence number
   * @return messages between fromSequenceNumber and nextSequence
   */
  std::vector<ByteBuffer> getMessages(int fromSequenceNumber);

  /**
   * Get next message in order and start timer for the message
   * @return next message if available
   */
  std::optional<ByteBuffer> getNextMessage();

  /**
   * Get message that have been timeouted
   * @return timeouted messages
   */
  std::vector<ByteBuffer> getTimeoutedMessages();

  /**
   * Set message generation function
   * <br><br>
   * <b>Signature: </b>
   * <code>ByteBuffer generate(int seqNumber)</code>
   * @param generateMessageFn the function
   */
  void setGenerateMessageFn(const std::function<ByteBuffer(int)> generateMessageFn);
 private:
  std::mutex messageMutex;
  std::map<int, ByteBuffer> storedMessages;

  std::map<int, milliseconds> sentMessageTimestamp;

  uint32_t size;
  int timeoutInMillis;

  int nextSequence{1};

  std::function<ByteBuffer(int)> generateMessageFn; // ByteBuffer generate(int seqNumber);
};

#endif
