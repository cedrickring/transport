#include <sstream>
#include "sender.h"

using namespace std::chrono;
using namespace std::chrono_literals;

/**
 * -------------------
 * | seq_nr | ack_nr | client -> server | 1 | 0 |
 * | 128-bit message | server -> client | 0 | 1 |
 * | checksum 64 bit |
 * -------------------
 * @param args
 */
Sender::Sender(std::vector<std::string> &args) : window(20), socket(0) { // set window size N to 20
  parseCommandArguments(args);
  lastSend = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
  socket.start();

  window.setGenerateMessageFn([](int seqNumber) {
    ByteBuffer buffer(144, 144);
    buffer.writeUInt32(seqNumber);
    buffer.writeUInt32(0); // ack number is not important here

    std::string message = "Hello " + std::to_string(transport::randomNumber(1, 100));
    buffer.writeString(message);
    buffer.fill(136); // fill payload with zeroes
    int checksum = transport::computeChecksum(buffer);
    buffer.writeUInt64(checksum);

    return buffer;
  });
}

void Sender::parseCommandArguments(std::vector<std::string> &args) {
  /* Args indices
   * 2 remotePort
   * 3 Message Count
   * 4 Message Interval
  */

  if (args.size() < 5) {
    std::cout
        << "Usage: ./transport sender <remotePort> <messageCount> <messageInterval>"
        << std::endl;
    std::exit(1);
  }

  remotePort = std::stoi(args[2]);
  messageCount = std::stoi(args[3]);
  messageInterval = std::stoi(args[4]);
}

void Sender::startSendLoop() {
  if (sending) {
    return;
  }

  sendingLoop = std::thread([this]() {
    sending = true;
    while (sending) {
      if (messagesSent == messageCount) {
        std::cout << "All messages have been sent" << std::endl;
        sending = false;
        break;
      }

      auto now = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
      auto diff = duration_cast<seconds>(now - lastSend);

      if (diff >= seconds(messageInterval)) {
        std::optional<ByteBuffer> buffer = window.getNextMessage();
        if (buffer.has_value()) {
          int seqNumber = buffer->readUInt32();
          socket.sendMessage("localhost", remotePort, *buffer);
          lastSend = now;

          ++messagesSent;
          std::cout << "> Sent next message with sequence number " << seqNumber << std::endl;
        }
      }

      std::this_thread::sleep_for(500ms);
    }
  });
}

void Sender::startReceiveLoop() {
  if (receiving) {
    return;
  }

  receiveLoop = std::thread([this]() {
    receiving = true;
    while (receiving) {
      std::optional<IncomingMessage> message = socket.receiveNextMessage();
      if (message.has_value()) {
        ByteBuffer &buffer = message->getBuffer();
        buffer.readUInt32(); // skip sequence number
        uint32_t ackNumber = buffer.readUInt32();
        buffer.readBytes(128); // skip payload
        uint64_t checksum = buffer.readUInt64();

        uint64_t computedChecksum = transport::computeChecksum(buffer);
        if (computedChecksum != checksum) {
          std::cout << "< Received corrupt package from " << message->getRemoteHost() << ":" << message->getRemotePort()
                    << std::endl;
          continue;
        }

        window.acknowledgeMessage(ackNumber);
      }
    }
  });
}

void Sender::startTimeoutLoop() {
  if (timeoutLoopRunning) {
    return;
  }

  timeoutLoop = std::thread([this]() {
    timeoutLoopRunning = true;

    while (timeoutLoopRunning) {
      std::vector<ByteBuffer> timeoutedBuffers = window.getTimeoutedMessages();
      if (!timeoutedBuffers.empty()) {
        int seqNumber = timeoutedBuffers[0].readUInt32();
        std::stringstream str;
        str << "> Resending messages: ";

        std::vector<ByteBuffer> messages = window.getMessages(seqNumber);
        for (auto &it : messages) {
          str << it.readUInt32() << " ";
          socket.sendMessage("localhost", remotePort, it);
        }
        str << std::endl;
        std::cout << str.str();
      } else if (timeoutedBuffers.empty() && messagesSent == messageCount) { // all messages have been successfully sent
        std::cout << "Nothing more to recover" << std::endl;
        break;
      }

      std::this_thread::sleep_for(2s);
    }
  });
}

void Sender::waitForTimeoutLoop() {
  timeoutLoop.join();
}

void Sender::stopTimeoutLoop() {
  timeoutLoopRunning = false;
}

void Sender::waitForSendLoop() {
  sendingLoop.join();
}

void Sender::stopSendLoop() {
  sending = false;
}

void Sender::stopReceiveLoop() {
  receiving = false;
  receiveLoop.join();
}
