#ifndef TRANSPORT_SRC_TRANSPORT_SENDER_SENDER_H_
#define TRANSPORT_SRC_TRANSPORT_SENDER_SENDER_H_

#include <vector>
#include <string>
#include <atomic>
#include <thread>
#include <chrono>
#include <map>

#include "server/udp/udp_server_socket.h"
#include "../util.h"
#include "window.h"

class Sender {

 public:
  /**
   * Create a new sender with command line arguments
   * @param args command line arguments
   */
  Sender(std::vector<std::string> &args);

  /**
   * Parse the command line arguments
   * @param args command line arguments
   */
  void parseCommandArguments(std::vector<std::string> &args);

  /**
   * Start send loop
   */
  void startSendLoop();

  /**
   * Start message timeout loop. All messages that are timeouted are sent again
   */
  void startTimeoutLoop();

  /**
   * Start receive loop for acknowledge messages
   */
  void startReceiveLoop();

  /**
   * Stop sending loop created in Sender#startSendLoop
   */
  void stopSendLoop();

  /**
   * Stop receive loop created in Sender#startReceiveLoop
   */
  void stopReceiveLoop();

  /**
   * Stop timeout loop created in Sender#startTimeoutLoop
   */
  void stopTimeoutLoop();

  /**
   * Wait for all messages to be sent
   */
  void waitForSendLoop();

  /**
   * Wait for all timeouted messages to be acknowledged
   */
  void waitForTimeoutLoop();
 private:
  uint16_t remotePort;
  int messageCount;
  int messageInterval;

  Window window;
  int messagesSent{0};

  UDPServerSocket socket;
  std::atomic<bool> sending{false};
  std::chrono::milliseconds lastSend;

  std::thread sendingLoop;

  std::atomic<bool> receiving{false};
  std::thread receiveLoop;

  std::atomic<bool> timeoutLoopRunning{false};
  std::thread timeoutLoop;
};

#endif
