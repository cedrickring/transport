#ifndef TRANSPORT_SRC_TRANSPORT_UTIL_H_
#define TRANSPORT_SRC_TRANSPORT_UTIL_H_

#include <random>
#include <numeric>
#include <vector>
#include <algorithm>
#include <csignal>

#include "common/byte_buffer.h"

namespace transport {

int randomNumber(int from, int to);

std::vector<int> nRandomNumbers(int n, int from, int to);

uint64_t computeChecksum(ByteBuffer& buffer);

/**
 * Add a signal handler for the given signum
 *
 * @see csignal
 * @param signum signal to handle
 * @param handler handler function
 */
void handleSignal(int signum, std::function<void(int)> handler);

void signalHandler(int signum); // wrapper for signalHandlerFunction to be accepted by signal

}

#endif
