#include "receiver.h"

Receiver::Receiver(std::vector<std::string> &args) {
  parseCommandArguments(args);
  socket = UDPServerSocket(port);
  socket.start();

}

void Receiver::parseCommandArguments(std::vector<std::string> &args) {
  /* Args indices
   * 2 Port
   * 3 Packet Loss Probability
   * 4 Corruption Probability
   */

  port = std::stoi(args[2]);
  packetLossProbability = std::stof(args[3]);
  corruptionProbability = std::stof(args[4]);
}

void Receiver::startReceiving() {
  receiveLoop = std::thread([this]() {
    isReceiving = true;

    std::cout << "Ready to receive messages" << std::endl;
    while (isReceiving) {
      std::optional<IncomingMessage> message = socket.receiveNextMessage();
      if (message.has_value()) {
        if (shouldLoosePacket()) {
          std::cout << "> Losing packet from " << message->getRemoteHost() << ":" << message->getRemotePort()
                    << std::endl;
          continue;
        }

        ByteBuffer &buffer = message->getBuffer();
        if (shouldCorrupt()) {
          std::cout << "> Corrupting packet" << std::endl;
          std::vector<int> corruptIndices = transport::nRandomNumbers(20, 0, 143); // packet length is 144

          // 136 Bytes? [6, 1, 66, 4, 22] ->
          for (int &i : corruptIndices) {
            buffer.setByte(i, static_cast<uint8_t>(transport::randomNumber(0, 255)));
          }
        }

        uint32_t sequenceNumber = buffer.readUInt32();
        uint32_t ackNumber = buffer.readUInt32();
        std::cout << "> Received message with sequence number: " << sequenceNumber << " and ack number: " << ackNumber
                  << std::endl;
        ByteBuffer packetBuffer = buffer.readBuffer(128);
        uint64_t checksum = buffer.readUInt64(); // 4 bytes nach message ist checksum

        uint64_t computedChecksum = transport::computeChecksum(buffer);
        if (computedChecksum != checksum) {
          std::cout << "> Received corrupt package from " << message->getRemoteHost() << ":" << message->getRemotePort()
                    << " (original: " << checksum << " != computed: " << computedChecksum << ")"
                    << std::endl;
          continue;
        }

        if (sequenceNumber > lastSuccessfulAck + 1) { // if it's not the next ack number
          std::cout << "- Dropping packet " << sequenceNumber << " as there are some missing packets between." << std::endl;
          sequenceNumber = lastSuccessfulAck; // resend last successful ack
        }

        //TODO check if lastSuccessfulAck is 0

        std::string receivedMessage = packetBuffer.readString();
        std::cout << "> Received message: " << receivedMessage << std::endl;

        ByteBuffer ackBuffer(144, 144);
        ackBuffer.writeUInt32(0);
        ackBuffer.writeUInt32(sequenceNumber);
        ackBuffer.writeString("ack");
        ackBuffer.fill(136); // fill empty space
        ackBuffer.writeUInt64(transport::computeChecksum(ackBuffer));

        socket.sendMessage(message->getRemoteHost(), message->getRemotePort(), ackBuffer);
        std::cout << "< Sent ack message for sequence " << sequenceNumber << std::endl;

        lastSuccessfulAck = sequenceNumber;
      }
    }
  });
}

void Receiver::stopReceiving() {
  isReceiving = false;
  receiveLoop.join();
}

bool Receiver::shouldCorrupt() {
  // fast paths
  if (corruptionProbability == 0) {
    return false;
  }
  if (corruptionProbability == 1) {
    return true;
  }

  int n = transport::randomNumber(1, 100);
  return n < static_cast<int>(std::round(corruptionProbability * 100));
}

bool Receiver::shouldLoosePacket() {
  // fast paths
  if (packetLossProbability == 0) {
    return false;
  }
  if (packetLossProbability == 1) {
    return true;
  }

  int n = transport::randomNumber(1, 100);
  return n < static_cast<int>(std::round(packetLossProbability * 100));
}



