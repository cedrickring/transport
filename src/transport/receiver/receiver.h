#ifndef TRANSPORT_SRC_TRANSPORT_RECEIVER_RECEIVER_H_
#define TRANSPORT_SRC_TRANSPORT_RECEIVER_RECEIVER_H_

#include <vector>
#include <string>
#include <atomic>
#include <thread>

#include "../util.h"
#include "server/udp/udp_server_socket.h"

class Receiver {

 public:
  /**
   * Create a new receiver with command line arguments
   * @param args command line arguments
   */
  explicit Receiver(std::vector<std::string> &args);

  /**
   * Parse the command line arguments
   * @param args command line arguments
   */
  void parseCommandArguments(std::vector<std::string> &args);

  /**
   * Start receive loop
   */
  void startReceiving();

  /**
   * Stop receive loop
   */
  void stopReceiving();
 private:
  uint16_t port;
  float packetLossProbability;
  float corruptionProbability;

  uint32_t lastSuccessfulAck{0};

  UDPServerSocket socket;
  std::atomic<bool> isReceiving{false};
  std::thread receiveLoop;

  bool shouldCorrupt();
  bool shouldLoosePacket();

};

#endif
