#ifndef ROUTING_SOCKETS_TCP_CLIENT_H_
#define ROUTING_SOCKETS_TCP_CLIENT_H_

#include <string>
#include <sys/socket.h>
#include <netdb.h>
#include <cstring>
#include <unistd.h>

#include "../../common/sender.h"
#include "../../common/receiver.h"
#include "../../common/util.h"

class TCPClient : public Sender, public Receiver {
 public:
  /**
   * Create a new tcp client for given host & port
   * @param host remote host
   * @param port remote port
   */
  TCPClient(std::string host, unsigned short port);

  /**
   * Connect to the tcp server
   */
  void connectToServer();

  /**
   * Terminate the connection
   */
  void stop();

  /**
   * Check if the client is connected
   * @return true, if the client is connected
   */
  bool isConnected();

 private:
  int fd;
  std::string host;
  unsigned short port;

};

#endif
