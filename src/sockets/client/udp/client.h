#ifndef ROUTING_SOCKETS_UDP_CLIENT_H_
#define ROUTING_SOCKETS_UDP_CLIENT_H_

#include <string>
#include <sys/socket.h>
#include <netdb.h>
#include <cstring>
#include <unistd.h>
#include <vector>

#include "../../common/util.h"
#include "../../common/exception.h"

class UDPClient {
 public:
  /**
   * Create a new udp client
   * @param host remote host
   * @param port remote port
   */
  UDPClient(std::string host, unsigned short port);

  /**
   * Receive a new message from the remote host
   * @param output will contain the received message
   * @return this
   */
  UDPClient &operator>>(std::string &output);

  /**
   * Send a message
   * @param message message to be sent
   * @return this
   */
  UDPClient &operator<<(std::string &message);

  /**
   * Send a message
   * @param message to be sent
   * @return this
   */
  UDPClient &operator<<(const char *message);

  /**
   * Close client
   */
  void stop();
 private:
  int fd;
  std::string host;
  unsigned short port;
  addrinfo *info;
};

#endif
