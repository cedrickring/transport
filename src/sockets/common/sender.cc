#include "sender.h"

Sender &Sender::operator<<(const char *message) {
  std::string messageStr = message;
  return operator<<(messageStr);
}

Sender &Sender::operator<<(std::string &message) {
  if (send(fd, message.c_str(), message.length(), 0) == -1) {
    throw SocketException("Failed to send message.");
  }

  return *this;
}