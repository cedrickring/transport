#ifndef ROUTING_EXCEPTION_H
#define ROUTING_EXCEPTION_H

#include <iostream>
#include <exception>
#include <utility>

class SocketException : public std::exception {
 public:
  explicit SocketException(std::string message) : message(std::move(message)) {
  }

  virtual const char *what() const noexcept {
    return message.c_str();
  }

 private:
  std::string message;
};

class SocketClosedException : public std::exception {
 public:
  explicit SocketClosedException(std::string message) : message(std::move(message)) {
  }

  virtual const char *what() const noexcept {
    return message.c_str();
  }

 private:
  std::string message;
};

class UnknownHostException : public std::exception {
 public:
  explicit UnknownHostException(std::string host, unsigned short port) {
    message = "Unknown host: " + host + ":" + std::to_string(port);
  }

  virtual const char *what() const noexcept {
    return message.c_str();
  }

 private:
  std::string message;
};

class EOFException : public std::exception {
 public:
  explicit EOFException(std::string message): message(std::move(message)) {
  }

  virtual const char *what() const noexcept {
    return message.c_str();
  }

 private:
  std::string message;
};

#endif //ROUTING_EXCEPTION_H
