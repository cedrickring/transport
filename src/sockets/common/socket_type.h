#ifndef ROUTING_SOCKETS_SOCKET_TYPE_H_
#define ROUTING_SOCKETS_SOCKET_TYPE_H_

/**
 * Used for socket creation
 */
enum SocketType {
  TCP,
  UDP
};

#endif //ROUTING_SOCKETS_SOCKET_TYPE_H_
