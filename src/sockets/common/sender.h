#ifndef ROUTING_SOCKETS_COMMON_SENDER_H_
#define ROUTING_SOCKETS_COMMON_SENDER_H_

#include <iostream>
#include <sys/socket.h>
#include "exception.h"

/**
 * Interface for send messages to a fd with stream semantics
 */
class Sender {
 public:
  explicit Sender() {}

  /**
   * Send a message to the fd
   * @param message to be sent
   * @return this
   */
  Sender &operator<<(std::string &message);


  /**
   * Send a message to the fd
   * @param message to be sent
   * @return this
   */
  Sender &operator<<(const char *message);

 protected:
  /**
   * Set fd of the socket to send from
   * @param newFd socket fd
   */
  void setFd(int newFd) {
    fd = newFd;
  }

 private:
  int fd;
};

#endif //ROUTING_SOCKETS_COMMON_SENDER_H_
