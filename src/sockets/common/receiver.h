#ifndef ROUTING_SOCKETS_COMMON_RECEIVER_H_
#define ROUTING_SOCKETS_COMMON_RECEIVER_H_

#include <iostream>
#include <vector>
#include <sys/socket.h>
#include "exception.h"

/**
 * Interface for receiving messages from a fd with stream semantics
 */
class Receiver {
 public:
  explicit Receiver() {}

  /**
   * Receive a message
   * @param output will contain the message
   * @return this
   */
  Receiver &operator>>(std::string &output);

 protected:
  /**
   * Set fd of the socket to receive from
   * @param newFd socket fd
   */
  void setFd(int newFd) {
    fd = newFd;
  }

 private:
  int fd;
};

#endif //ROUTING_SOCKETS_COMMON_SENDER_H_
