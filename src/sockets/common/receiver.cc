#include "receiver.h"

Receiver &Receiver::operator>>(std::string &output) {
  output.clear();

  const unsigned int MAX_BUF_LENGTH = 4096;
  std::vector<char> buffer(MAX_BUF_LENGTH);

  // read incoming message into "output"
  int read;
  do {
    read = recv(fd, &buffer[0], buffer.size(), 0);
    if (read == -1) {
      throw SocketException("Failed to read bytes.");
    } else {
      output.append(buffer.cbegin(), buffer.cend());
    }
  } while (read == MAX_BUF_LENGTH);

  return *this;
}
