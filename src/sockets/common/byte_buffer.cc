#include "byte_buffer.h"
#include <numeric>

ByteBuffer::ByteBuffer(std::vector<uint8_t> bytes, int maxLength)
    : bytes(std::move(bytes)), readIndex(0), maxLength(maxLength) {
}

ByteBuffer::ByteBuffer(int initialCapacity, int maxLength) : bytes(0), readIndex(0), maxLength(maxLength) {
  bytes.reserve(initialCapacity);
}

size_t ByteBuffer::availableBytes() {
  return bytes.size() - readIndex;
}

void ByteBuffer::resetReadIndex() {
  readIndex = 0;
}

void ByteBuffer::writeString(const std::string &string) {
  checkWrite(string.length() + 4);
  writeUInt32(string.length());
  std::copy(string.begin(), string.end(), std::back_inserter(bytes));
}

std::string ByteBuffer::readString() {
  uint32_t length = readUInt32();
  std::string str(bytes.begin() + readIndex, bytes.begin() + readIndex + length);
  readIndex += length;
  return str;
}

void ByteBuffer::writeInt8(int8_t i, size_t position) {
  checkWrite(1);
  if (position == 0) {
    bytes[position] = i;
  } else {
    bytes.push_back(i);
  }
}

void ByteBuffer::writeInt16(int16_t i, size_t position) {
  checkWrite(2);
  if (position == 0) {
    bytes.push_back((i >> 8) & 0xFF);
    bytes.push_back((i >> 0) & 0xFF);
  } else {
    bytes[position] = (i >> 8) & 0xFF;
    bytes[position + 1] = (i >> 0) & 0xFF;
  }
}

void ByteBuffer::writeInt32(int32_t i, size_t position) {
  checkWrite(4);
  if (position == 0) {
    bytes.push_back((i >> 24) & 0xFF);
    bytes.push_back((i >> 16) & 0xFF);
    bytes.push_back((i >> 8) & 0xFF);
    bytes.push_back((i >> 0) & 0xFF);
  } else {
    bytes[position] = ((i >> 24) & 0xFF);
    bytes[position + 1] = ((i >> 16) & 0xFF);
    bytes[position + 2] = ((i >> 8) & 0xFF);
    bytes[position + 3] = ((i >> 0) & 0xFF);
  }
}

void ByteBuffer::writeInt64(int64_t i, size_t position) {
  checkWrite(8);
  if (position == 0) {
    bytes.push_back((i >> 56) & 0xFF);
    bytes.push_back((i >> 48) & 0xFF);
    bytes.push_back((i >> 40) & 0xFF);
    bytes.push_back((i >> 32) & 0xFF);
    bytes.push_back((i >> 24) & 0xFF);
    bytes.push_back((i >> 16) & 0xFF);
    bytes.push_back((i >> 8) & 0xFF);
    bytes.push_back((i >> 0) & 0xFF);
  } else {
    bytes[position] = ((i >> 56) & 0xFF);
    bytes[position + 1] = ((i >> 48) & 0xFF);
    bytes[position + 2] = ((i >> 40) & 0xFF);
    bytes[position + 3] = ((i >> 32) & 0xFF);
    bytes[position + 4] = ((i >> 24) & 0xFF);
    bytes[position + 5] = ((i >> 16) & 0xFF);
    bytes[position + 6] = ((i >> 8) & 0xFF);
    bytes[position + 7] = ((i >> 0) & 0xFF);
  }
}

void ByteBuffer::writeUInt8(uint8_t i, size_t position) {
  checkWrite(1);
  if (position == 0) {
    bytes[position] = i;
  } else {
    bytes.push_back(i);
  }
}

void ByteBuffer::writeUInt16(uint16_t i, size_t position) {
  checkWrite(2);
  if (position == 0) {
    bytes.push_back((i >> 8) & 0xFF);
    bytes.push_back((i >> 0) & 0xFF);
  } else {
    bytes[position] = (i >> 8) & 0xFF;
    bytes[position + 1] = (i >> 0) & 0xFF;
  }
}

void ByteBuffer::writeUInt32(uint32_t i, size_t position) {
  checkWrite(4);
  if (position == 0) {
    bytes.push_back((i >> 24) & 0xFF);
    bytes.push_back((i >> 16) & 0xFF);
    bytes.push_back((i >> 8) & 0xFF);
    bytes.push_back((i >> 0) & 0xFF);
  } else {
    bytes[position] = ((i >> 24) & 0xFF);
    bytes[position + 1] = ((i >> 16) & 0xFF);
    bytes[position + 2] = ((i >> 8) & 0xFF);
    bytes[position + 3] = ((i >> 0) & 0xFF);
  }
}

void ByteBuffer::writeUInt64(uint64_t i, size_t position) {
  checkWrite(8);
  if (position == 0) {
    bytes.push_back((i >> 56) & 0xFF);
    bytes.push_back((i >> 48) & 0xFF);
    bytes.push_back((i >> 40) & 0xFF);
    bytes.push_back((i >> 32) & 0xFF);
    bytes.push_back((i >> 24) & 0xFF);
    bytes.push_back((i >> 16) & 0xFF);
    bytes.push_back((i >> 8) & 0xFF);
    bytes.push_back((i >> 0) & 0xFF);
  } else {
    bytes[position] = ((i >> 56) & 0xFF);
    bytes[position + 1] = ((i >> 48) & 0xFF);
    bytes[position + 2] = ((i >> 40) & 0xFF);
    bytes[position + 3] = ((i >> 32) & 0xFF);
    bytes[position + 4] = ((i >> 24) & 0xFF);
    bytes[position + 5] = ((i >> 16) & 0xFF);
    bytes[position + 6] = ((i >> 8) & 0xFF);
    bytes[position + 7] = ((i >> 0) & 0xFF);
  }
}

int8_t ByteBuffer::readInt8() {
  checkRead(1);
  return bytes.at(readIndex++);
}

int16_t ByteBuffer::readInt16() {
  checkRead(2);
  uint8_t i1 = readUInt8();
  uint8_t i2 = readUInt8();
  return (i1 << 8) + (i2 << 0);
}

int32_t ByteBuffer::readInt32() {
  checkRead(4);
  uint8_t i1 = readUInt8();
  uint8_t i2 = readUInt8();
  uint8_t i3 = readUInt8();
  uint8_t i4 = readUInt8();
  return (i1 << 24) + (i2 << 16) + (i3 << 8) + (i4 << 0);
}

int64_t ByteBuffer::readInt64() {
  checkRead(8);
  uint8_t i1 = readUInt8();
  uint8_t i2 = readUInt8();
  uint8_t i3 = readUInt8();
  uint8_t i4 = readUInt8();
  uint8_t i5 = readUInt8();
  uint8_t i6 = readUInt8();
  uint8_t i7 = readUInt8();
  uint8_t i8 = readUInt8();
  return ((uint64_t) i1 << 56) + ((uint64_t) (i2 & 255) << 48) + ((uint64_t) (i3 & 255) << 40)
      + ((uint64_t) (i4 & 255) << 32)
      + ((uint32_t) (i5 & 255) << 24)
      + ((uint32_t) (i6 & 255) << 16) + ((i7 & 255) << 8) + ((i8 & 255) << 0);
}

uint8_t ByteBuffer::readUInt8() {
  checkRead(1);
  return bytes.at(readIndex++);
}

uint16_t ByteBuffer::readUInt16() {
  checkRead(2);
  uint8_t i1 = readUInt8();
  uint8_t i2 = readUInt8();
  return (i1 << 8) + (i2 << 0);
}

uint32_t ByteBuffer::readUInt32() {
  checkRead(4);
  uint8_t i1 = readUInt8();
  uint8_t i2 = readUInt8();
  uint8_t i3 = readUInt8();
  uint8_t i4 = readUInt8();
  return (i1 << 24) + (i2 << 16) + (i3 << 8) + (i4 << 0);
}

uint64_t ByteBuffer::readUInt64() {
  checkRead(8);
  uint8_t i1 = readUInt8();
  uint8_t i2 = readUInt8();
  uint8_t i3 = readUInt8();
  uint8_t i4 = readUInt8();
  uint8_t i5 = readUInt8();
  uint8_t i6 = readUInt8();
  uint8_t i7 = readUInt8();
  uint8_t i8 = readUInt8();
  return ((uint64_t) i1 << 56) + ((uint64_t) (i2 & 255) << 48) + ((uint64_t) (i3 & 255) << 40)
      + ((uint64_t) (i4 & 255) << 32)
      + ((uint32_t) (i5 & 255) << 24)
      + ((uint32_t) (i6 & 255) << 16) + ((i7 & 255) << 8) + ((i8 & 255) << 0);
}

template<int len>
std::array<uint8_t, len> ByteBuffer::readByteArray() {
  checkRead(len);
  std::array<uint8_t, len> array;
  std::copy(bytes[readIndex], bytes[readIndex + len], array.begin());
  return array;
}

std::vector<uint8_t> ByteBuffer::readBytes(int length) {
  checkRead(length);
  std::vector<uint8_t> vec(bytes.begin() + readIndex, bytes.begin() + readIndex + length);
  readIndex += length;
  return vec;
}

ByteBuffer ByteBuffer::readBuffer(int length) {
  return ByteBuffer(readBytes(length));
}

const std::vector<uint8_t> &ByteBuffer::getBytes() const {
  return bytes;
}

void ByteBuffer::checkRead(size_t length) {
  if (readIndex + length > bytes.size()) {
    throw EOFException(
        "checkRead: Reading " + std::to_string(length) + " bytes exceeds buffer length: "
            + std::to_string(bytes.size()));
  }
}

void ByteBuffer::checkWrite(size_t length) {
  if (maxLength == -1) {
    return;
  }

  if (bytes.size() + length > static_cast<size_t>(maxLength)) {
    throw EOFException(
        "checkWrite: Buffer size " + std::to_string(bytes.size()) + " + " + std::to_string(length)
            + " exceeds max length of "
            + std::to_string(maxLength));
  }
}

void ByteBuffer::fill(size_t until) {
  std::fill_n(std::back_inserter(bytes), until - bytes.size(), 0);
}

void ByteBuffer::setByte(size_t index, uint8_t value) {
  bytes[index] = value;
}

