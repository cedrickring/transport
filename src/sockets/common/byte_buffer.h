#ifndef TRANSPORT_SRC_TRANSPORT_PACKETS_BYTE_BUFFER_H_
#define TRANSPORT_SRC_TRANSPORT_PACKETS_BYTE_BUFFER_H_

#include <string>
#include <vector>
#include <array>

#include "exception.h"

class ByteBuffer {

 public:
  explicit ByteBuffer() = default;
  explicit ByteBuffer(int initialCapacity, int maxLength);
  explicit ByteBuffer(std::vector<uint8_t> bytes, int maxLength = -1);

  size_t availableBytes();

  void resetReadIndex();

  void writeString(const std::string &string);
  std::string readString();

  void writeInt8(int8_t i, size_t position = 0);
  void writeInt16(int16_t i, size_t position = 0);
  void writeInt32(int32_t i, size_t position = 0);
  void writeInt64(int64_t, size_t position = 0);

  void writeUInt8(uint8_t i, size_t position = 0);
  void writeUInt16(uint16_t i, size_t position = 0);
  void writeUInt32(uint32_t i, size_t position = 0);
  void writeUInt64(uint64_t i, size_t position = 0);

  void setByte(size_t index, uint8_t value);

  int8_t readInt8();
  int16_t readInt16();
  int32_t readInt32();
  int64_t readInt64();

  uint8_t readUInt8();
  uint16_t readUInt16();
  uint32_t readUInt32();
  uint64_t readUInt64();

  void fill(size_t toBytes);

  template<int len>
  std::array<uint8_t, len> readByteArray();

  std::vector<uint8_t> readBytes(int length);

  ByteBuffer readBuffer(int length);

  const std::vector<uint8_t> &getBytes() const;

 private:
  std::vector<uint8_t> bytes{};
  uint32_t readIndex{0};
  int maxLength{-1};

  void checkRead(size_t length);
  void checkWrite(size_t length);

};

#endif
