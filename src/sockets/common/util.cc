#include "util.h"

namespace sockets {

void lookupHost(std::string host, unsigned short port, addrinfo **result) {
  struct addrinfo hints;
  std::memset(&hints, 0, sizeof(hints));

  hints.ai_family = AF_UNSPEC;
  hints.ai_protocol = 0;
  hints.ai_flags = AI_ADDRCONFIG;
  hints.ai_socktype = SOCK_DGRAM;

  int err;
  if ((err = getaddrinfo(host.c_str(), std::to_string(port).c_str(), &hints, result)) != 0) {
    throw SocketException("Unable to lookup IP address: " + std::string(gai_strerror(err)));
  }
}

}
