#include "socket.h"

ServerSocket::ServerSocket(SocketType type): type(type) {
}

void ServerSocket::start() {
  int sockType = type == SocketType::TCP ? SOCK_STREAM : SOCK_DGRAM;
  fd = socket(AF_INET, sockType, 0);
  if (fd == -1) {
    throw SocketException("Failed to create server");
  }
}

void ServerSocket::stopGracefully() {
  if (fd != -1) {
    std::cout << "Stopping server..." << std::endl;
    close(fd);
    fd = -1;
    std::cout << "Stopped server." << std::endl;
  }
}

