#ifndef ROUTING_SOCKET_H
#define ROUTING_SOCKET_H

#include <iostream>
#include <arpa/inet.h>
#include <cerrno>
#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <functional>

#include "../common/exception.h"
#include "../common/socket_type.h"

class ServerSocket {

 public:
  ServerSocket() = default;
  /**
   * Create a new server with given type
   * @param type type of socket (udp / tcp)
   */
  explicit ServerSocket(SocketType type);

  /**
   * Start the server
   */
  virtual void start();

  /**
   * Stop the server
   */
  virtual void stopGracefully();

 protected:
  SocketType type{TCP};
  int fd{};

};

#endif
