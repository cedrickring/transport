#ifndef ROUTING_CONNECTED_CLIENT_H
#define ROUTING_CONNECTED_CLIENT_H

#include <iostream>
#include <utility>
#include <netinet/in.h>
#include <unistd.h>
#include "../../common/exception.h"
#include "../../common/receiver.h"
#include "../../common/sender.h"

class ConnectedClient : public Sender, public Receiver {
 public:
  /**
   * Create a new connected client of a tcp server
   * @param fd fd of the client
   * @param remoteHost host of the client
   * @param remotePort port of the client
   */
  ConnectedClient(int fd, std::string remoteHost, unsigned short remotePort);

  /**
   * Get the client's host
   * @return host
   */
  const std::string &getRemoteHost() const;

  /**
   * Get the client's port
   * @return port
   */
  unsigned short getRemotePort() const;

 private:
  int fd;
  std::string remoteHost;
  unsigned short remotePort;

};

#endif
