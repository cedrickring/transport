#include "connected_client.h"

ConnectedClient::ConnectedClient(int fd, std::string remoteHost, unsigned short remotePort)
    : fd(fd), remoteHost(std::move(remoteHost)), remotePort(remotePort) {
}

const std::string &ConnectedClient::getRemoteHost() const {
  return remoteHost;
}

unsigned short ConnectedClient::getRemotePort() const {
  return remotePort;
}
