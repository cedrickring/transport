#include "tcp_server_socket.h"

TCPServerSocket::TCPServerSocket(unsigned short port, int backlog)
    : ServerSocket(SocketType::TCP), backlog(backlog), port(port) {
}

void TCPServerSocket::start() {
  ServerSocket::start();

  struct sockaddr_in addr{};
  addr.sin_addr.s_addr = INADDR_ANY;
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);

  if (bind(fd, (const struct sockaddr *) &addr, sizeof(addr)) == -1) {
    throw SocketException("Failed to bind to port " + std::to_string(port));
  }

  if (listen(fd, backlog) == -1) {
    throw SocketException("Failed to listen on port " + std::to_string(port));
  }
}

ConnectedClient TCPServerSocket::acceptClient() const {
  if (type == SocketType::UDP) {
    throw SocketException("UDP client cannot accept clients is undefined for UDP sockets");
  }

  sockaddr_in clientAddr{};
  socklen_t clientAddrLen = sizeof(clientAddr);

  int client = accept(fd, (sockaddr *) &clientAddr, &clientAddrLen);
  if (client == -1) {
    if (errno == EBADF) { // server was closed
      throw SocketClosedException("Server server is closed.");
    }
    throw SocketException("Couldn't except new client.");
  }

  return ConnectedClient(client, inet_ntoa(clientAddr.sin_addr), ntohs(clientAddr.sin_port));
}
