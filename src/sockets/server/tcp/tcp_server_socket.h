#ifndef ROUTING_SOCKETS_SERVER_TCP_SERVER_SOCKET_H_
#define ROUTING_SOCKETS_SERVER_TCP_SERVER_SOCKET_H_

#include "../socket.h"
#include "connected_client.h"

class TCPServerSocket : public ServerSocket {

 public:
  explicit TCPServerSocket(unsigned short port, int backlog = 10);

  virtual void start() override;

  ConnectedClient acceptClient() const;

 private:
  int backlog;
  unsigned short port;
};

#endif
