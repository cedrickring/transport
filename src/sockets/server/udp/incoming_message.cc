#include "incoming_message.h"

IncomingMessage::IncomingMessage(ByteBuffer buffer,
                                 std::string remoteHost,
                                 unsigned int remotePort)
    : buffer(std::move(buffer)), remoteHost(std::move(remoteHost)), remotePort(remotePort) {
}

std::string IncomingMessage::getMessage() const {
  return std::string(buffer.getBytes().begin(), buffer.getBytes().end());
}

ByteBuffer &IncomingMessage::getBuffer() {
  return buffer;
}

const std::string &IncomingMessage::getRemoteHost() const {
  return remoteHost;
}

unsigned short IncomingMessage::getRemotePort() const {
  return remotePort;
}