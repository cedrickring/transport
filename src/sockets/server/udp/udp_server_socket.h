#ifndef ROUTING_SOCKETS_SERVER_UDP_SERVER_SOCKET_H_
#define ROUTING_SOCKETS_SERVER_UDP_SERVER_SOCKET_H_

#include "../socket.h"
#include "../../common/util.h"
#include "incoming_message.h"
#include <variant>

class UDPServerSocket : public ServerSocket {
 public:
  UDPServerSocket() = default;

  /**
   * Create a new udp socket to bind on the specified port
   * @param port port to bind on
   * @param receiveTimeout recvfrom block timeout in ms
   */
  explicit UDPServerSocket(unsigned short port, int receiveTimeout = 500);

  virtual void start() override;

  /**
   * Receive the next incoming udp message
   * @return incoming message if a message was received during the timeout period
   */
  std::optional<IncomingMessage> receiveNextMessage();

  /**
   * Send a message to a udp client
   * @param foreignHost foreign host
   * @param foreignPort foreign port
   * @param message message to be sent
   */
  void sendMessage(std::string foreignHost, unsigned int foreignPort, std::variant<ByteBuffer, std::string> message);

  /**
   * Get actual port of the socket
   * Should be used when initialized with port 0
   * @return actual port
   */
  int getPort();
 private:
  int receiveTimeout{0};
  unsigned int port{0};

};

#endif
