#include "udp_server_socket.h"
#include <cerrno>

UDPServerSocket::UDPServerSocket(unsigned short port, int receiveTimeout)
    : ServerSocket(SocketType::UDP), receiveTimeout(receiveTimeout), port(port) {
}

void UDPServerSocket::start() {
  ServerSocket::start();

  if (receiveTimeout > 0) {
    timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = receiveTimeout * 100;
    if (setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) == -1) {
      throw SocketException("Couldn't set receive timeout on server");
    }
  }

  sockaddr_in serverAddr;
  std::memset(&serverAddr, 0, sizeof(serverAddr));
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(port);
  serverAddr.sin_addr.s_addr = INADDR_ANY;

  if (bind(fd, (sockaddr *) &serverAddr, sizeof(serverAddr)) == -1) {
    throw SocketException("Failed to bind to port");
  }

}

int UDPServerSocket::getPort() {
  sockaddr_in sin;
  socklen_t len = sizeof(sin);

  if (getsockname(fd, (struct sockaddr *)&sin, &len) == -1) {
    return -1;
  }
  return ntohs(sin.sin_port);
}


void UDPServerSocket::sendMessage(std::string foreignHost, unsigned int foreignPort, std::variant<ByteBuffer, std::string> message) {
  addrinfo *info;
  sockets::lookupHost(foreignHost, foreignPort, &info);

  std::string msg;

  if (std::holds_alternative<std::string>(message)) {
    msg = std::get<std::string>(message);
  } else {
    auto& buffer = std::get<ByteBuffer>(message);
    msg = std::string(buffer.getBytes().begin(), buffer.getBytes().end());
  }

  if (sendto(fd, msg.c_str(), msg.length(), 0, info->ai_addr, info->ai_addrlen) == -1) {
    throw SocketException("Failed to send message to " + foreignHost + ":" + std::to_string(foreignPort));
  }
}

std::optional<IncomingMessage> UDPServerSocket::receiveNextMessage() {
  sockaddr_in clientAddr;
  socklen_t clientAddrLen;
  clientAddrLen = sizeof(clientAddr);
  std::memset(&clientAddr, 0, sizeof(clientAddr));

  const unsigned int MAX_BUF_LENGTH = 4096;
  std::vector<uint8_t> buffer(MAX_BUF_LENGTH);

  int read = recvfrom(fd, &buffer[0], MAX_BUF_LENGTH, 0, (sockaddr *) &clientAddr, &clientAddrLen);
  if (read == -1) {
    if (errno == EAGAIN || errno == EINTR) { // ignore error if interrupted / timeout
      return {};
    }
    throw SocketException("Failed to read from udp server");
  }

  std::string remoteHost = inet_ntoa(clientAddr.sin_addr);
  unsigned short remotePort = ntohs(clientAddr.sin_port);

  return IncomingMessage(ByteBuffer(buffer), remoteHost, remotePort);
}

