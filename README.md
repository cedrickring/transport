# Assignment 3: Transport

## Prerequisites

* Linux/Unix with POSIX-style sockets and pthreads (libstdc++, libpthread, libgcc), tested on Ubuntu 20.04
* CMake 3.10+
* G++ with C++17 Support (I'm using 9.3.0)
* (Optional) Ninja for building

**NOTE**: g++-7 won't be able to compile this code!

To install g++-9 use:

```
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo apt update
sudo apt install gcc-9 g++-9
```

And set `CC` and `CXX` to the correct compiler before running `cmake`:
```
export CC=gcc-9
export CXX=g++-9
```

## Build

With `make`:
```
mkdir build && cd build
cmake -G"Unix Makefiles" ..
make
cp src/transport/transport ../
```

With `ninja`
```
mkdir build && cd build
cmake -GNinja ..
ninja
cp src/transport/transport ../
```

## Usage

First start a receiver with: `./transport receiver 60000 0.1 0.4`.
Arguments are:
* Port `60000`
* Packet Loss Probability `0.1`
* Corruption Probability `0.4`

Then start a sender with `./transport sender 60000 30 1`
Arguments are:
* Port of the receiver: `60000`
* Number of messages: `30`
* Time between messages: `1`